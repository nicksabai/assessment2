(function () {
    angular.module("MyGroceryApp")
        .config(GroceryConfig);

    GroceryConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function GroceryConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("list", {
                url: "/list",
                templateUrl: "/views/list.html",
                controller: "ListCtrl as ctrl"
            })

        $urlRouterProvider.otherwise("/list");
    }

    
})();