(function () {
    angular.module("MyGroceryApp")
        .controller("ListCtrl", ListCtrl);

    ListCtrl.$inject = ['GroceryService', '$state'];

        
    function ListCtrl(GroceryService, $state) {
        var list = this;
        list.products = "";
        list.typesOfSearch = ['Brand','Name'];
        list.searchType = [];
        list.searchType.selectedType = [];
        list.sortBy = "";
        list.keyword = "";

        list.totalItems = 0;
        list.itemsPerPage = 20;
        list.maxSize = 8;
        list.currentPage = 1;

        list.pageChanged = function() {
            console.log('Page changed to: ' + list.currentPage);

            GroceryService.search(list.searchType, list.keyword, list.sortBy, list.itemsPerPage, list.currentPage)
                .then(function (products) {
                    list.products = products;
                    list.totalItems = products.length;
                }).catch(function (err) {
                console.info("Some Error Occurred",err)
            });
        };

        list.search = function (searchType, keyword, sortBy) {
            if(searchType.length==0) {
                alert('Please select a search type');
            }
            else {
                list.searchType = searchType;
                list.keyword = keyword;
                GroceryService.search(searchType, keyword, sortBy, list.itemsPerPage, list.currentPage)
                    .then(function (products) {
                        console.log(products);
                        list.products = products;
                        list.totalItems = products.length;
                    })
                    .catch(function (err) {
                        console.info("Some Error Occurred",err);
                    });
            }
        };
        
        list.getProduct = function (id) {
            $state.go("edit", {'productId' : id});
        };

        GroceryService.search(list.searchType, list.keyword, list.sortBy, list.itemsPerPage, list.currentPage)
            .then(function (products) {
                list.products = products;
                list.totalItems = products.length;
            }).catch(function (err) {
            console.info("Some Error Occurred",err)
        });
    }

})();