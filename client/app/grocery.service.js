(function () {
    
    angular.module("MyGroceryApp")
            .service("GroceryService", GroceryService);

    GroceryService.$inject = ['$http', '$q'];        
    
    function GroceryService($http, $q) {
        var grocery = this;
        
        grocery.search = function (searchType, keyword, sortBy, items, page) {
            var defer = $q.defer();
            var params = {
                searchType: searchType,
                keyword: keyword,
                sortBy: sortBy,
                items: items,
                page: page
            };
            $http.get("/api/products", {
                params: params
                }).then(function (results) {
                    console.log(results.data)
                    defer.resolve(results.data);
                }).catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
    }
    
})();