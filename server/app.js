/**
 * Server side code.
 */
//Load libraries
console.log("Starting...");
const express = require("express");
const path = require("path");
const mysql = require("mysql");
const q = require("q");

//Create an instance of express
const app = express();

//Configure mysql pool
const pool = mysql.createPool({
	host: "localhost", 
	port: 3306,
	user: "root", 
	password: "",
	database: "grocery",
	connectionLimit: 4
});

const mkQQuery = function(sql, connPool) {

	const sqlQuery = function() {
		const defer = q.defer();

		console.log("Using q package");
		
		//Collect arguments
		var sqlParams = [];
		for (var i in arguments)
			sqlParams.push(arguments[i])

		connPool.getConnection(function(err, conn) {
			if (err) {
				defer.reject(err);
				return;
			}
			conn.query(sql, sqlParams, function(err, result) {
				if (err)
					defer.reject(err);
				else
					defer.resolve(result);
				conn.release();
			})
		});

		return (defer.promise);
	}

	return (sqlQuery);
}

//List of SQL commands
const SEARCH_GROCERIES = "select * from grocery_list ";
const GET_GROCERY = "select * from grocery_list where id=?";

const getGrocery = mkQQuery(GET_GROCERY, pool);

const handleError = function(err, resp) {
	resp.status(500);
	resp.type("application/json");
	resp.json(err);
}

app.get("/api/products", function(req, resp) {
	var result = {}
	var whereCondition = "";
    var page = parseInt(req.query.page) || 1;
    var items = parseInt(req.query.items) || 10;
    var offset = (page - 1) * items;
    var limit = items;
    var sortBy = req.query.sortBy || 'ASC';
    var brand = '';
    var name = '';
    var order = 'name '+sortBy;

    console.log(req.query.searchType);

    if((typeof req.query.searchType !== 'undefined')) {
        if(typeof req.query.searchType === 'string'){
            if(req.query.searchType=='Brand') {
                brand = req.query.keyword;
                order = 'brand '+sortBy;
            }
            if(req.query.searchType=='Name') {
                name = req.query.keyword;
            }
        } else {
            brand = req.query.keyword;
            name = req.query.keyword;
        }
    }

    if(brand && name) {
		whereCondition = " where brand like '%" 
						+ brand + "%' OR name like '%" 
						+ name + "%' " + " ORDER BY " + order + " LIMIT " + limit 
						+ ' OFFSET ' + offset;
    } else {
        if(brand) {
			whereCondition = " where brand like '%" 
			+ brand + "%'" + " ORDER BY " + order + " LIMIT " + limit 
			+ ' OFFSET ' + offset;
        }
        else if(name) {
            whereCondition = " where name like '%" 
			+ name + "%'" + " ORDER BY " + order + " LIMIT " + limit 
			+ ' OFFSET ' + offset;
        }
        else {
            whereCondition = " ORDER BY " + order + " LIMIT " + limit 
			+ ' OFFSET ' + offset;
        }
	}
	console.log(SEARCH_GROCERIES + whereCondition);
	const searchGroceries = mkQQuery(SEARCH_GROCERIES + whereCondition, pool);
	searchGroceries().then(function(result){
		resp.json(result);
	}).catch(function(error){
		console.log(error);
	});

	
});


app.get("/api/products/:productId", function(req, resp) {
	var result = {
		
	}
	resp.json(result);
	resp.end();
});

//Set the static route
app.use(express.static(path.join(__dirname, "../client")));

//configure the port
console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

// Start the server
app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});
